package com.hendisantika.resttemplate;

import com.hendisantika.resttemplate.service.RestfulClient;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringRestfulApiForTransferImageApplication {

    public static void main(String[] args) {
        /*
         *POST ENTITY
         */

        RestfulClient restfulClient = new RestfulClient();

        restfulClient.postEntity();

        /*
         * GET ENTITY
         */
        restfulClient.getEntity();
    }
}