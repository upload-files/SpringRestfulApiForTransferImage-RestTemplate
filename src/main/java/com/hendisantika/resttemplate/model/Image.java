package com.hendisantika.resttemplate.model;

/**
 * Created by IntelliJ IDEA.
 * Project : SpringRestfulApiForTransferImage-RestTemplate
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 18/07/18
 * Time: 20.30
 * To change this template use File | Settings | File Templates.
 */
public class Image {
    private String name;
    private String data;

    public Image() {
    }

    public Image(String name, String data) {
        this.name = name;
        this.data = data;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    @Override
    public String toString() {
        String info = String.format("Image2 name = %s, data = %s", name, data);
        return info;
    }
}
