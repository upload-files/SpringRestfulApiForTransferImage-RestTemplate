package com.hendisantika.resttemplate.service;

import com.hendisantika.resttemplate.model.Image;
import com.hendisantika.resttemplate.util.UtilBase64Image;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

/**
 * Created by IntelliJ IDEA.
 * Project : SpringRestfulApiForTransferImage-RestTemplate
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 18/07/18
 * Time: 20.31
 * To change this template use File | Settings | File Templates.
 */
public class RestfulClient {
    RestTemplate restTemplate;

    public RestfulClient() {
        restTemplate = new RestTemplate();
    }

    /**
     * post entity
     */
    public void postEntity() {
        System.out.println("Begin /POST request!");
        String postUrl = "http://localhost:8080/post";
        String name = "demoImage.png";
        String imagePath = "C:\\client\\demoImage.png";
        String data = UtilBase64Image.encoder(imagePath);

        System.out.println("Post Image2'info: name = " + name + " ,data = " + data);
        Image customer = new Image(name, data);

        ResponseEntity<String> postResponse = restTemplate.postForEntity(postUrl, customer, String.class);
        System.out.println("Response for Post Request: " + postResponse.getBody());
    }

    /**
     * get entity
     */
    public void getEntity() {
        System.out.println("Begin /GET request!");
        String getUrl = "http://localhost:8080/get?name=demoImage.png";
        ResponseEntity<Image> getResponse = restTemplate.getForEntity(getUrl, Image.class);

        if (getResponse.getBody() != null) {
            Image image = getResponse.getBody();
            System.out.println("Response for Get Request: " + image.toString());
            System.out.println("Save Image2 to C:\\client\\get");
            UtilBase64Image.decoder(image.getData(), "C:\\client\\get\\" + image.getName());
            System.out.println("Done!");
        } else {
            System.out.println("Response for Get Request: NULL");
        }
    }
}
